#include "pch.h"

#pragma warning(disable:4996)

namespace common {
	// 文件是否存在
	bool is_file(char* filename)
	{
		FILE* f = fopen(filename, "r");
		if (f) {
			fclose(f);
			return true;
		}
		return false;
	}

	// 实现无缓存的下载页面
	STDAPI download(IN PCH url, OUT PCH local_path)
	{
		char tmp[300];
		wsprintfA(tmp, "%s?t=%d", url, GetTickCount());
		return URLDownloadToCacheFileA(0, tmp, local_path, MAX_PATH, 0, NULL);
	}

	// 使用浏览器打开网址
	void open_url(PCH url)
	{
		ShellExecute(NULL, _T("open"), url, NULL, NULL, SW_SHOW);
	}


	// 释放资源
	BOOL ReleaseRes(PCH strFileName, WORD wResID, PCH strFileType)
	{
		// 资源大小  
		DWORD dwWrite = 0;

		// 创建文件  
		HANDLE  hFile = CreateFileA(strFileName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL,
			CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hFile) {
			// 查找资源文件中、加载资源到内存、得到资源大小  
			HRSRC   hrsc = FindResourceA(NULL, MAKEINTRESOURCEA(wResID), strFileType);
			HGLOBAL hG = LoadResource(NULL, hrsc);
			DWORD   dwSize = SizeofResource(NULL, hrsc);

			// 写入文件  
			WriteFile(hFile, hG, dwSize, &dwWrite, NULL);
			CloseHandle(hFile);
		}

		return TRUE;
	}


	// 强制结束程序
	BOOL end_process(DWORD pid)
	{
		HANDLE h_Process = OpenProcess(PROCESS_TERMINATE, FALSE, pid);
		if (h_Process)
			return TerminateProcess(h_Process, 0);
		return FALSE;
	}

	// 运行进程
	BOOL run_exe(PCH szFile, PCH szCmd, int ishow)
	{
		char dir[200];
		lstrcpyA(dir, szFile);
		for (int i = strlen(dir) - 1; i > 0; i--) {
			if (dir[i] == '\\') {
				dir[i + 1] = 0;
				break;
			}
		}
		//MessageBoxA( 0, dir, 0, 0 );

		char openstr[] = { 0x4F,0x70,0x65,0x6E,0 };//Open

		SHELLEXECUTEINFO sei;
		sei.cbSize = sizeof(sei);//设置类型大小。
		sei.hwnd = 0; //命令窗口进程句柄，ShellExecuteEx函数执行时设置。
		sei.lpVerb = openstr;//执行动作为“打开执行”
		sei.lpFile = szFile;//执行程序文件全路径名称。
		sei.lpParameters = szCmd;//执行参数。
		sei.lpDirectory = dir;
		sei.nShow = ishow; //显示方式，此处使用隐藏方式阻止出现命令窗口界面。
		sei.fMask = SEE_MASK_NOCLOSEPROCESS; //设置为SellExecuteEx函数结束后进程退出。

		//创建执行命令窗口进程。
		if (ShellExecuteEx(&sei)) {
			//设置命令行进程的执行级别为空闲执行，这使本程序有足够的时间从内存中退出。
			//SetPriorityClass(sei.hProcess,IDLE_PRIORITY_CLASS);

			//通知Windows资源浏览器，本程序文件已经被删除。
			//SHChangeNotify(SHCNE_DELETE,SHCNF_PATH,szModule,0);
			//执行退出程序。
			return 1;
		}

		return 0;
	}


	//
	//使用createprocess隐藏执行system
	void system(char* command)
	{
		STARTUPINFOA si;
		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);

		char buf[200];
		wsprintfA(buf, "cmd.exe /c %s", command);
		PROCESS_INFORMATION pi;
		BOOL res = CreateProcessA(NULL,
			buf, // 执行你的 dos 命令
			NULL, NULL, NULL,
			NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,
			NULL, NULL, &si, &pi);

		if (TRUE == res) {
			WaitForSingleObject(pi.hProcess, INFINITE);

			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
		}
	}


	// 获取进程
	DWORD find_process(PCH pname)
	{
		PROCESSENTRY32 me32;
		me32.dwSize = sizeof(PROCESSENTRY32);
		HANDLE hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hModuleSnap == INVALID_HANDLE_VALUE)
			return 0;

		if (Process32First(hModuleSnap, &me32)) {
			do {
				if (lstrcmpiA(me32.szExeFile, pname) == 0) {
					return me32.th32ProcessID;
				}
			} while (Process32Next(hModuleSnap, &me32));
		}

		CloseHandle(hModuleSnap);
		return 0;
	}

}