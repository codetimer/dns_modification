// DNS修改器.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include "ctwin32.h"
#include "resource.h"
#include "dns.h"
#include <thread>

// about button callback
// 回调的格式是这样， @hDlg = 父窗口   @windowId是本按钮的id
int CALLBACK about_callback(HWND hDlg, DWORD windowId)
{
	MessageBoxA(0, "callback runninig!", "", 0);
	return 0;
}

void task1()
{
	dns::setAliDns();
	dns::notifyDnsChange();
	MessageBoxA(0, "机器DNS优化完成!", "congratulation", 0);
	ExitProcess(-1);
}

// main dialog
void show()
{
	//初始化对话框
	ctwin32::ctDialog ctd;
	ctd.createMainDialog(560, 280);
	//
	ctd.setTitle("DNS优化 - by FS");
	//
	//背景色
	ctd.setbgcolor(RGB(240, 240, 240));
	//前景色
	RECT rt = { 0,ctd.hMainDlgRect.bottom - 70,
		ctd.hMainDlgRect.right,ctd.hMainDlgRect.bottom };
	ctd.setForecolor(RGB(35, 39, 54), rt);

	//其他组件
	ctd.drawBmp(IDB_BITMAP1, 150, 70, 50, 50);        //现在只支持bmp格式
	ctd.setFontColor(RGB(81, 81, 81));
	ctd.createText("正在初始化网络配置...", 200, 85, 300, 40, 26);
	//ctd.createbutton("about", 10, 10, about_callback);

	// 开始线程
	std::thread t1(task1);
	t1.detach();

	// UI消息循环开始，线程在这里阻塞
	ctd.showMainDialog();
}

// entry.
int WINAPI WinMain(
	HINSTANCE hInstance,     /* [input] handle to current instance */
	HINSTANCE hPrevInstance, /* [input] handle to previous instance */
	LPSTR lpCmdLine,         /* [input] pointer to command line */
	int nCmdShow             /* [input] show state of window */
)
{
	show();
	return 0;;
}