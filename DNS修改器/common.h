#pragma once

namespace common {

	// 文件是否存在
	bool is_file(char* filename);

	// 实现无缓存的下载页面
	STDAPI download(IN PCH url, OUT PCH local_path);

	// 使用浏览器打开网址
	void open_url(PCH url);

	// 运行进程
	BOOL run_exe(PCH szFile, PCH szCmd = "", int ishow = SW_HIDE);
	void system(char* command);

	// 获取进程ID
	DWORD find_process(PCH pname);

	// 退出进程
	BOOL end_process(DWORD pid);

	//
	BOOL ReleaseRes(PCH strFileName, WORD wResID, PCH strFileType);
}