#include "pch.h"

#include "common.h"
#include "dns.h"
#include <string>

using namespace std;

namespace dns {

	//
	// set currently DNS to AliDNS
	void setAliDns()
	{
		char buf[300] = {0};
		dns::getLanAdapterName(buf);
		// MessageBoxA(0, buf, 0, 0);
		for (int i = 0; i < 300; i++) {
			if (buf[i] == '/' || buf[i] == 0) {
				char name[200];
				buf[i] = 0;
				lstrcpyA(name, buf);
				if(lstrlenA(name)>1)
					dns::setDns(name, "223.5.5.5", "");
				lstrcpyA(buf, buf + i+1);
			}
		}
	}


	//
	// 重启网卡
	void notifyDnsChange()
	{
		//WinXp+
		common::system("netsh interface set interface name=\"本地连接\" admin=DISABLED");
		common::system("netsh interface set interface name=\"本地连接\" admin=ENABLED");
		//Win10
		common::system("netsh interface set interface name=\"以太网\" admin=DISABLED");
		common::system("netsh interface set interface name=\"以太网\" admin=ENABLED");
	}


	//
	// 读取注册表取得适配器名称  
	void getLanAdapterName(char* szLanAdapterName)
	{
		HKEY hKey, hSubKey, hNdiIntKey;
		if (RegOpenKeyExA(HKEY_LOCAL_MACHINE,
			"System\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}",
			0, KEY_READ, &hKey) != ERROR_SUCCESS) {
			return;
		}

		DWORD dwIndex = 0;
		DWORD dwBufSize = 256;
		DWORD dwDataType;
		char szSubKey[256];
		unsigned char szData[256];
		while (RegEnumKeyExA(hKey, dwIndex++, szSubKey, &dwBufSize, NULL, NULL, NULL, NULL) == ERROR_SUCCESS) {
			if (RegOpenKeyExA(hKey, szSubKey, 0, KEY_READ, &hSubKey) == ERROR_SUCCESS) {
				if (RegOpenKeyExA(hSubKey, "Ndi\\Interfaces", 0, KEY_READ, &hNdiIntKey) == ERROR_SUCCESS) {
					dwBufSize = 256;
					if (RegQueryValueExA(hNdiIntKey, "LowerRange", 0, &dwDataType, szData, &dwBufSize) == ERROR_SUCCESS) {
						if (strcmp((char*)szData, "ethernet") == 0) // 判断是不是以太网卡  
						{
							dwBufSize = 256;
							if (RegQueryValueExA(hSubKey, "DriverDesc", 0, &dwDataType, szData, &dwBufSize) == ERROR_SUCCESS) {
								// printf( "%s\n",szData );
								// szData 中便是适配器详细描述  
								dwBufSize = 256;
								if (RegQueryValueExA(hSubKey, "NetCfgInstanceID", 0, &dwDataType, szData, &dwBufSize) == ERROR_SUCCESS) {
									//szData中便是适配器名称  , 使用 / 串联
									if (lstrlenA(szLanAdapterName) < 1) {
										lstrcpyA(szLanAdapterName, (const char *)szData);
									} else {
										lstrcatA(szLanAdapterName, "/");
										lstrcatA(szLanAdapterName, (const char *)szData);
									}
									// break;
								}
							}
						}
					}
					RegCloseKey(hNdiIntKey);
				}
				RegCloseKey(hSubKey);
			}
			dwBufSize = 256;
		}
		RegCloseKey(hKey);
	}


	//
	// 修改设置
	BOOL setDns(PCH lpszAdapterName, PCH pDNSServer1, PCH pDNSServer2)
	{
		HKEY hKey;
		string strKeyName = "SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\";
		strKeyName += lpszAdapterName;
		if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, strKeyName.c_str(), 0, KEY_READ | KEY_WRITE, &hKey) != ERROR_SUCCESS)
			return FALSE;

		string DNSServer = pDNSServer1;
		DNSServer += ",";
		DNSServer += pDNSServer2;
		char mszDNSServer[100];
		strncpy_s(mszDNSServer, DNSServer.c_str(), 98);

		int nDNSServer = strlen(mszDNSServer);
		*(mszDNSServer + nDNSServer + 1) = 0x00;


		char tmp[200] = "";
		DWORD tt = REG_SZ;
		DWORD ss = 200;
		RegQueryValueExA(hKey, "NameServer", 0, &tt, (unsigned char *)tmp, &ss);
		if (lstrcmpiA(DNSServer.c_str(), tmp) == 0) {
			// it has been set
			return TRUE;
		}

		RegSetValueExA(hKey, "NameServer", 0, REG_SZ, (unsigned char*)mszDNSServer, nDNSServer);
		RegCloseKey(hKey);
		//
		notifyDnsChange();
		return TRUE;
	}


}